## Необходимо реализовать класс или структуру классов, позволяющую создать связанный список с помощью ES6 (или более поздних версий) со следующими свойствами и методами:
1.	Элементы могут быть представлены различными типами 
    `constructor(comparatorFunction: (a: NodeType, b: NodeType) => number)`

2.	Должна быть возможность добавить элемент в начало списка
    `prepend(value: NodeType): LinkedList<NodeType>`

3.	Должна быть возможность добавить элемент в конец списка
    `append(value: NodeType): LinkedList<NodeType>`

4.	Должна быть возможность удалить элемент
    `delete(value: NodeType): (LinkedListNode<NodeType> | null)`

5.	Должна быть возможность найти элемент по значению
    `find(value: FindParams): (LinkedListNode<NodeType> | null)`

6.	Должна быть возможность удалить последнй элемент
    `deleteTail(): (LinkedListNode<NodeType> | null)`

7.	Должна быть возможность удалить первый элемент
    `deleteHead(): (LinkedListNode<NodeType> | null)`

8.	Должна быть возможность создать список из массива
    `fromArray(values: Array<NodeType>): LinkedList<NodeType>`

9.	Должна быть возможность создать массив из списка
    `toArray(): Array<NodeType>`

10.	Должна быть возможность развернуть список
    `reverse(): LinkedList<NodeType>`

11.	Должна быть возможность преобразовать список в строку
    `toString(callback: (value: any) => string): string`
